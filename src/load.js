const fs = require('fs');
const Promise = require('bluebird');
const request = require('request-promise');
const ftp = require('ftp-get');

function loadByFs(path) {
    return Promise.promisify(fs.readFile)(path, 'base64');
}

function loadByFtp(path) {
    return new Promise((resolve, reject) => {
        ftp.get({
            url: path,
            bufferType: 'buffer'
        }, (err, res) => {
            if (err) return reject(err);
            resolve(res.toString('base64'));
        });
    });
}

function loadByHttp(path) {
    return request({
        uri: path,
        encoding: 'base64'
    });
}

function getDefaultMethod(type) {
    return (path) => {
        return new Promise((resolve, reject) => {
            reject(new Error(`load image ${path} failed. Not support type: ${type}`));
        });
    };
}

exports.getLoadMethod = (type) => {
    const methodTable = {
        fs: loadByFs,
        ftp: loadByFtp,
        http: loadByHttp
    };
    return methodTable[type] || getDefaultMethod(type);
};

exports.getLoadType = (path) => {
    if (path.startsWith('http')) return 'http';
    if (path.startsWith('ftp')) return 'ftp';
    return 'fs';
};
