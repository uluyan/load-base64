const load = require('./load');

/**
 * @param {string} path
 * @param {string} type
 */
exports.loadBase64 = (path, type) => {
    type = type || load.getLoadType(path);
    return load.getLoadMethod(type)(path);
};
