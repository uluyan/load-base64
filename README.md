# load-base64
load file encoding base64

read local file or download file by ftp/http uri, encoding base64.

## Installation

This module is installed via npm:
```
npm install load-base64
```

## Usage

```js
const fs = require('fs');
const loadBase64 = require('load-base64').loadBase64;

loadBase64('ftp://node:nodejs@127.0.0.1:21/test/test.jpg')
    .then((res) => {
        fs.writeFileSync('test.jpg', res, 'base64');
        console.log('ok');
    })
    .catch((err) => {
        console.log(`load failed. ${err.message}`);
    });

```

argument `path` could be ftp/http uri or local file path, and you can set path type manually by argument `type`.

*this is also a exercise for mocha, should.js and istanbul.*
