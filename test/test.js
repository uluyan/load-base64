require('should');
const crypto = require('crypto');
const path = require('path');
const cases = require('./case').cases;
const loadImage = require('../src').loadImage;

function digest(msg) {
    return crypto.createHash('md5').update(msg).digest('hex');
}

suite('normal', () => {
    cases.normal.forEach((_) => {
        test(_.name, (done) => {
            loadImage(..._.input)
                .then((output) => {
                    digest(output).should.equal(_.output);
                    done();
                })
                .catch(done)
        });
    });
});

suite('ivalid', () => {
    cases.ivalid.forEach((_) => {
        test(_.name, (done) => {
            loadImage(..._.input)
                .then(() => {
                    done(new Error(`should throw an error`));
                })
                .catch((err) => {
                    err.message.should.match(new RegExp(_.output));
                    done();
                })
                .catch(done);
        });
    });
});
